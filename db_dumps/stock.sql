-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               10.0.19-MariaDB - mariadb.org binary distribution
-- Server Betriebssystem:        Win64
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Exportiere Struktur von Tabelle exchange.stock
CREATE TABLE IF NOT EXISTS `stock` (
  `stock_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`stock_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- Exportiere Daten aus Tabelle exchange.stock: ~1 rows (ungefähr)
/*!40000 ALTER TABLE `stock` DISABLE KEYS */;
INSERT INTO `stock` (`stock_id`, `name`) VALUES
	(1, 'Adidas'),
	(2, 'Allianz'),
	(3, 'BASF'),
	(4, 'Bayer'),
	(5, 'Beiersdorf'),
	(6, 'BMW'),
	(7, 'Commerzbank'),
	(8, 'Continental'),
	(9, 'Daimler'),
	(10, 'Deutsche Bank'),
	(11, 'Deutsche Börse'),
	(12, 'Deutsche Post'),
	(13, 'Deutsche Telekom'),
	(14, 'E.ON'),
	(15, 'Fresenius Medical Care'),
	(16, 'Fresenius'),
	(17, 'HeidelbergCement'),
	(18, 'Henkel'),
	(19, 'Infineon Technologies'),
	(20, 'K+S'),
	(21, 'Lanxess'),
	(22, 'Linde'),
	(23, 'Lufthansa'),
	(24, 'Merck'),
	(25, 'Munich RE'),
	(26, 'RWE'),
	(27, 'SAP'),
	(28, 'Siemens'),
	(29, 'ThyssenKrupp'),
	(30, 'Volkswagen');
/*!40000 ALTER TABLE `stock` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
