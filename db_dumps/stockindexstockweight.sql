-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server Version:               10.0.19-MariaDB - mariadb.org binary distribution
-- Server Betriebssystem:        Win64
-- HeidiSQL Version:             9.2.0.4947
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Exportiere Struktur von Tabelle exchange.stockindexstockweight
CREATE TABLE IF NOT EXISTS `stockindexstockweight` (
  `stock_index_stock_weight_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `weight` double DEFAULT NULL,
  `stock_id` bigint(20) DEFAULT NULL,
  `stock_index_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`stock_index_stock_weight_id`),
  KEY `FK_t46ydpwfpbl7ep5j5ypysm7bc` (`stock_id`),
  KEY `FK_ok816yio2laefo1asjtk8cmwk` (`stock_index_id`),
  CONSTRAINT `FK_ok816yio2laefo1asjtk8cmwk` FOREIGN KEY (`stock_index_id`) REFERENCES `stockindex` (`stock_index_id`),
  CONSTRAINT `FK_t46ydpwfpbl7ep5j5ypysm7bc` FOREIGN KEY (`stock_id`) REFERENCES `stock` (`stock_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- Exportiere Daten aus Tabelle exchange.stockindexstockweight: ~0 rows (ungefähr)
/*!40000 ALTER TABLE `stockindexstockweight` DISABLE KEYS */;
INSERT INTO `stockindexstockweight` (`stock_index_stock_weight_id`, `weight`, `stock_id`, `stock_index_id`) VALUES
	(1, 1.63, 1, 1),
	(2, 7.38, 2, 1),
	(3, 8.5, 3, 1),
	(4, 9.84, 4, 1),
	(5, 0.86, 5, 1),
	(6, 3.57, 6, 1),
	(7, 1.22, 7, 1),
	(8, 2.55, 8, 1),
	(9, 9.26, 9, 1),
	(10, 3.95, 10, 1),
	(11, 1.54, 11, 1),
	(12, 2.86, 12, 1),
	(13, 5.42, 13, 1),
	(14, 2.75, 14, 1),
	(15, 2.5, 15, 1),
	(16, 1.81, 16, 1),
	(17, 1.12, 17, 1),
	(18, 2.01, 18, 1),
	(19, 1.43, 19, 1),
	(20, 0.61, 20, 1),
	(21, 0.54, 21, 1),
	(22, 3.35, 22, 1),
	(23, 0.6, 23, 1),
	(24, 1.33, 24, 1),
	(25, 2.74, 25, 1),
	(26, 1.09, 26, 1),
	(27, 6.62, 27, 1),
	(28, 8.04, 28, 1),
	(29, 1.15, 29, 1),
	(30, 3.74, 30, 1);
/*!40000 ALTER TABLE `stockindexstockweight` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
