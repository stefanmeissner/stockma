package controllers;

import models.Stock;
import models.StockIndex;
import models.StockIndexStockWeight;
import play.data.Form;
import play.db.jpa.JPA;
import play.mvc.*;

import views.html.*;

import java.util.HashMap;
import java.util.List;

import javax.persistence.TypedQuery;

public class Application extends Controller {

    public Result index() {

        return ok(index.render("Hello World."));
    }

    public Result dashboard() {
        return ok(dashboard.render());
    }

    @play.db.jpa.Transactional
    public Result stockIndexes() {

        // get by Id
        // List<StockIndex> stockIndexes = new ArrayList<>();
        // stockIndexes.add(JPA.em().find(StockIndex.class, 1L));

        // get all with HQL
        TypedQuery<StockIndexStockWeight> typedQuery = JPA.em().createQuery("FROM StockIndexStockWeight", StockIndexStockWeight.class);
        List<StockIndexStockWeight> stockIndexes = typedQuery.getResultList();

        return ok(stockindex.render(scala.collection.JavaConversions.asScalaBuffer(stockIndexes)));
    }

}
