package controllers;

import models.Stock;
import models.StockPrice;
import play.data.Form;
import play.db.jpa.JPA;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.stock;

import javax.persistence.TypedQuery;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class StockController extends Controller {

    private Form<Stock> stockForm = Form.form(Stock.class);

    @play.db.jpa.Transactional
    private Map<String, String> getStockMap() {
        // get all with HQL
        TypedQuery<Stock> typedQuery = JPA.em().createQuery("FROM Stock", Stock.class);
        List<Stock> stockList = typedQuery.getResultList();

        Map<String, String> stockMap = new LinkedHashMap<>();
        for(Stock stock : stockList) {
            stockMap.put(stock.Id.toString(), stock.name);
        }

        return stockMap;
    }

    @play.db.jpa.Transactional
    private List<StockPrice> getStockPrices(long stockId) {
        // get all with HQL
        TypedQuery<StockPrice> typedQuery = JPA.em().createQuery("FROM StockPrice", StockPrice.class);
        List<StockPrice> stockPriceList = typedQuery.getResultList();

        return stockPriceList;
    }

    @play.db.jpa.Transactional
    public Result select() {

        return ok(stock.render(stockForm, getStockMap(), null));
    }

    @play.db.jpa.Transactional
    public Result edit() {
        long stockId = Long.parseLong(Form.form().bindFromRequest().get("Id"));

        List<StockPrice> stockPriceList = getStockPrices(stockId);

        Stock dummyStock = new Stock();
        dummyStock.name = "Test";

        StockPrice dummyStockPrice = new StockPrice();
        dummyStockPrice.stock = dummyStock;
        dummyStockPrice.price = 35l;
        stockPriceList.add(dummyStockPrice);

        return ok(stock.render(stockForm, getStockMap(), scala.collection.JavaConversions.asScalaBuffer(stockPriceList)));
    }
}
