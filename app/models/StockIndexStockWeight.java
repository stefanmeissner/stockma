package models;

import javax.persistence.*;

@Entity
public class StockIndexStockWeight {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "stock_index_stock_weight_id")
    public Long Id;

    @ManyToOne
    @JoinColumn(name = "stock_id")
    public Stock stock;

    @ManyToOne
    @JoinColumn(name = "stock_index_id")
    public StockIndex stockIndex;


    public Double weight;
}
