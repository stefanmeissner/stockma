package models;

import javax.persistence.*;

@Entity
public class StockPrice {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "stock_price_id")
    public Long Id;

    @ManyToOne
    @JoinColumn(name = "stock_id")
    public Stock stock;
    public java.util.Calendar timestamp;
    public long price;
}
