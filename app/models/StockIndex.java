package models;

import org.springframework.data.repository.CrudRepository;

import javax.persistence.*;

@Entity
public class StockIndex {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "stock_index_id")
    public Long Id;

    public String name;
}
