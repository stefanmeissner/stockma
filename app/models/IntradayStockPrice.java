package models;

import javax.persistence.*;

@Entity
public class IntradayStockPrice {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "intraday_stock_price_id")
    public Long Id;

    @ManyToOne
    @JoinColumn(name = "stock_id")
    private Stock stock;
    private java.util.Calendar timestamp;
    private long price;
}
