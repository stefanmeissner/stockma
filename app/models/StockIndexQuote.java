package models;

import javax.persistence.*;

@Entity
public class StockIndexQuote {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "stock_index_quote_id")
    public Long Id;

    @ManyToOne
    @JoinColumn(name = "stock_index_id")
    private StockIndex stockIndex;
    private java.util.Calendar timestamp;
    private long quote;
}
